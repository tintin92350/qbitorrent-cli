"""This module provides the qbb-cli config functionality."""
import configparser
import os

# qbcli/config.py

# Where the config file of qbitorrent is stored
QBITORRENT_CONFIG_DIR_PATH = os.getenv("QBITORRENT_CONFIG_DIR_PATH")
QBITORRENT_CONFIG_FILENAME = os.getenv("QBITORRENT_CONFIG_FILENAME", "qBittorrent.ini")
QBITORRENT_CONFIG_FILE_PATH = f"{QBITORRENT_CONFIG_DIR_PATH}/{QBITORRENT_CONFIG_FILENAME}"

# Where the credentials for VPN are stored
VPN_CREDENTIALS_DIR_PATH = os.getenv("VPN_CREDENTIALS_DIR_PATH")
VPN_CREDENTIALS_FILENAME = os.getenv("VPN_CREDENTIALS_FILENAME", "vpn-credentials.json")


def init_app() -> None:
    """Initialize the config app."""
    _init_config_file()


def _init_config_file() -> None:
    """Initialize the config file."""

    # Read the config file
    with open(QBITORRENT_CONFIG_FILE_PATH, "r") as config_file:
        config_file_content = config_file.read()

        # Parse the configuration file content with configparser
        global qbittorrent_data

        qbittorrent_data = configparser.ConfigParser()
        qbittorrent_data.read_string(config_file_content)


# Function that updates the config file
def update_config_file() -> None:
    """Update the config file."""

    # Create a backup of the config file
    with open(QBITORRENT_CONFIG_FILE_PATH, "r") as config_file:
        config_file_content = config_file.read()
        with open(f"{QBITORRENT_CONFIG_FILE_PATH}.bak", "w") as config_file_backup:
            config_file_backup.write(config_file_content)

    with open(QBITORRENT_CONFIG_FILE_PATH, "w") as config_file:
        qbittorrent_data.write(config_file)


# Function that restores the config file from the backup
def restore_config_file() -> int:
    """Restore the config file from the backup."""

    # If the backup file does not exists
    if not os.path.exists(f"{QBITORRENT_CONFIG_FILE_PATH}.bak"):
        return 1

    with open(f"{QBITORRENT_CONFIG_FILE_PATH}.bak", "r") as config_file_backup:
        config_file_backup_content = config_file_backup.read()
        with open(QBITORRENT_CONFIG_FILE_PATH, "w") as config_file:
            config_file.write(config_file_backup_content)

    os.remove(f"{QBITORRENT_CONFIG_FILE_PATH}.bak")

    return 0