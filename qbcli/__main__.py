"""qb-cli entry point script."""
# qbcli/__main__.py

from qbcli import main, __app_name__


def main_h():
    main.app(prog_name=__app_name__)


if __name__ == "__main__":
    main_h()
