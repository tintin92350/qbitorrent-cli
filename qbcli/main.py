"""This module provides the qb-cli."""
# qbcli/cli.py

from typing import Optional

import typer

from qbcli import __app_name__, __version__, config

config.init_app()

app = typer.Typer()


def _version_callback(value: bool) -> None:
    if value:
        typer.echo(f"{__app_name__} v{__version__}")
        raise typer.Exit()


@app.callback()
def main(
        version: Optional[bool] = typer.Option(
            None,
            "--version",
            "-v",
            help="Show the application's version and exit.",
            callback=_version_callback,
            is_eager=True,
        )
) -> None:
    return


@app.command(
    name="get-network-config",
    help="Display the network configuration.",
)
def get_network_config() -> None:
    """Get the config value."""

    # For all entry in network section
    for key in config.qbittorrent_data["Network"]:
        value: str = config.qbittorrent_data["Network"][key]
        typer.echo(f"{key}: {value}")


@app.command(
    name="set-network-config",
    help="Set the network configuration.",
)
def set_network_config(
        key: str,
        value: str,
) -> None:
    """Set the config value."""

    config.qbittorrent_data["Network"][key] = value
    config.update_config_file()


@app.command(
    name="restore-config",
    help="Restore the configuration.",
)
def restore_config() -> None:
    """Restore the config value."""

    if config.restore_config_file() == 1:
        typer.echo("Backup file not found.")
        raise typer.Exit(1)