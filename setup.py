#!/usr/bin/env python

from distutils.core import setup

setup(name='qbcli',
      version='1.0',
      description='CMI for qBittorrent',
      author='Quentin RODIC',
      author_email='quentin.rodic.pro@outlook.fr',
      packages=['qbcli'],
     )
